import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {observable, Observable} from 'rxjs';
import {User} from '../user';

@Injectable({
  providedIn: 'root'
})
export class LoginService 
{
  URL = "http://localhost:52726/api/User/";

  constructor(private http:HttpClient) { }

  isLoggedIn(email:string,password:string):Observable<User>
  {
    return this.http.get<User>(this.URL + email + "/" + password);
  }
}
