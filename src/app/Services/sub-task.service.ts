import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {observable, Observable} from 'rxjs';
import {Subtask} from '../subtask';

@Injectable({
  providedIn: 'root'
})
export class SubTaskService {
  URL = "http://localhost:52726/api/SubTask/"
  constructor(private http:HttpClient) { }

  getSubTask(taskId: number): Observable<Subtask[]>
  {
    var customurl = "http://localhost:52726/api/SubTask/Get/" + taskId.toString();
    return this.http.get<Subtask[]>(customurl);
  }
  markAllAsComplete(taskId: number):Observable<boolean>
  {
    return this.http.get<boolean>(this.URL + "GetMarkasComplete/" + taskId);
  }
  createSubTask(subTask: Subtask):Observable<boolean>
  {
    let httpHeaders = new HttpHeaders().set('Content-Type','application/json');
    let options={
      headers: httpHeaders
    }
    return this.http.post<boolean>(this.URL,subTask,options);
  }
}
