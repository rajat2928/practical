import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {observable, Observable} from 'rxjs';
import {Task} from '../task';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {

  URL = "http://localhost:52726/api/Task/"
  constructor(private http:HttpClient) { }
  getAllTask(userid: number):Observable<Task[]>{
    return this.http.get<Task[]>(this.URL + '/GetAllTask/' + userid);
  }
  markAllAsComplete(taskId: number):Observable<boolean>
  {
    return this.http.get<boolean>(this.URL + "GetMarkasComplete/" + taskId);
  }
  createTask(task: Task):Observable<boolean>
  {
    let httpHeaders = new HttpHeaders().set('Content-Type','application/json');
    let options={
      headers: httpHeaders
    }
    return this.http.post<boolean>(this.URL,task,options);
  }
}
