import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SubTaskComponent } from './sub-task/sub-task.component';
import {TaskListComponent} from './task-list/task-list.component'
const routes: Routes = [
  {
    path:'register',
    component: RegisterComponent
  },
  {
    path:'subtask',
    component: SubTaskComponent
  },
  {
    path:'tasklist',
    component: TaskListComponent
    
  },
  {
    path:'**',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
