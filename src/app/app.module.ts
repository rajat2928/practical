import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {RegistrationServiceService} from  './register/registration-service.service';
import { TaskListComponent } from './task-list/task-list.component'
import { TaskServiceService } from './Services/task-service.service';
import { LoginService } from './Services/login.service';
import { SubTaskService } from './Services/sub-task.service';
import { SubTaskComponent } from './sub-task/sub-task.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    TaskListComponent,
    SubTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [RegistrationServiceService, TaskServiceService, HttpClientModule, LoginService, SubTaskService],
  bootstrap: [AppComponent],

})
export class AppModule { }
