import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
userName: string;
loggedIn: boolean=false;
  constructor() { }

  ngOnInit(): void {
    this.isLoggedIn();
  }
  isLoggedIn()
  {
    this.userName = sessionStorage.getItem("UserName");
    if(this.userName==null || this.userName=="")
    {
      this.loggedIn=false;   
    }
    else
      this.loggedIn=true;
  } 
}
