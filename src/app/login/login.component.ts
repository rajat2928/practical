import { Component, OnInit } from '@angular/core';
import{FormBuilder,Validators, FormGroup} from '@angular/forms';
import{observable, Observable} from 'rxjs'
import {User} from '../user'
import {Router} from '@angular/router'
import { LoginService } from '../Services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit  {
loginUser: any;
loginForm:any;
errormessage: string;
userName: string;

constructor(private formbuilder:FormBuilder, private router: Router, private loginService: LoginService) { }

  ngOnInit(): void 
  {
    this.isLoggedIn();

    this.loginForm = this.formbuilder.group({
      Email: ['',[Validators.required]],
      Password: ['',[Validators.required]]
      });
  }
    //Method Create User on clicking Submit button
  public onFormSubmit(){
    let user = new User();
    user.Name = this.loginForm.value.Name;
    user.Email = this.loginForm.value.Email;
    user.Password = this.loginForm.value.Password;
    this.login(user);
  }
  public login(user: User)
  {

    this.loginService.isLoggedIn(user.Email, user.Password).subscribe(item=>{
        sessionStorage.setItem("UserName", item.Name);
        sessionStorage.setItem("UserID",item.Id.toString());
        this.refreshPage();
    });
  }
  refreshPage() {
    window.location.reload();
   }
   isLoggedIn()
   {
     if(sessionStorage.getItem("UserName")==null || sessionStorage.getItem("UserName")=="")
     {
        console.log("Not Logged In.");
     }
     else
       this.router.navigate([('/tasklist')]);
   } 
}
