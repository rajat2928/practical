import { Component, OnInit } from '@angular/core';
import{FormBuilder,Validators, FormGroup} from '@angular/forms';
import{RegistrationServiceService} from './registration-service.service';
import{User} from '../user'
import{observable, Observable} from 'rxjs'
import{Router} from '@angular/router'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
//#region 
  userForm:any;
  userName: string;
  alluser:any;
//#endregion
  constructor(private formbuilder:FormBuilder, private router: Router, private registrationservice: RegistrationServiceService) { }

  ngOnInit(): void {
    //this.isLoggedIn();
    this.getAllUser();
    this.userForm = this.formbuilder.group({
      Name: ['',[Validators.required]],
      Email: ['',[Validators.required]],
      Password: ['',[Validators.required]]
      });

  }
  //Reset Form
  resetForm(){
    this.userForm.reset();
  }
  onFormSubmit(){
    let user = this.userForm.value;
    this.createUser(user);
    this.resetForm();
  }
  createUser(user: User)
  {
    debugger
    this.registrationservice.createUser(user).subscribe(
      ()=>{
        alert("Successfully Registered");
        if(this.userName==null)
        {
          this.router.navigate(['/login']);     
        }
      })

  }
  getAllUser()
  {
    this.alluser = this.registrationservice.getAllUser();
  }
  //Check user login
 /*  isLoggedIn()
  {
    this.userName = localStorage.getItem("UserName");
    if(this.userName==null)
    {
      this.router.navigate(['/login']);     
    }
  } */
}
