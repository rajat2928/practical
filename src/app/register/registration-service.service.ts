import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {observable, Observable} from 'rxjs';
import {User} from '../user';

@Injectable({
  providedIn: 'root'
})
export class RegistrationServiceService  {
  //#region Variables
  url='http://localhost:52726/api/User';
  customURL:string;
  //#endregion
 
 //#region Constructor
  constructor(private http:HttpClient) {}
  //#endregion
  
  //#region API Methods

  //Create User
  createUser(user: User):Observable<User>{
    console.log("Create User: Begin");
    let httpHeaders = new HttpHeaders()
                        .set('Content-Type','application/json');
    let options={
      headers: httpHeaders
    }
    return this.http.post<User>(this.url,user,options);
  }
  getAllUser():Observable<User[]>{
    return this.http.get<User[]>(this.url);
  }
  //#endregion
}