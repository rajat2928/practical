import { Component, OnInit } from '@angular/core';
import{FormBuilder,Validators, FormGroup} from '@angular/forms';
import{observable, Observable} from 'rxjs'
import {Router} from '@angular/router'
import { SubTaskService } from '../Services/sub-task.service';
import{Subtask} from  '../subtask'
@Component({
  selector: 'app-sub-task',
  templateUrl: './sub-task.component.html',
  styleUrls: ['./sub-task.component.css']
})
export class SubTaskComponent implements OnInit {
  public isCollapsed = true;
  subTasks:Observable<Subtask[]>;
  subtaskForm: any;
  allTask:Observable<Subtask[]>;
  taskName = sessionStorage.getItem("taskname");

  constructor(private formbuilder:FormBuilder, private router: Router, private subtaskservice: SubTaskService) { }

  ngOnInit(): void {
    this.getSubTask(Number(sessionStorage.getItem("taskid")));
    this.subtaskForm = this.formbuilder.group({
      Title: ['',[Validators.required]],
      Description: ['',[Validators.required]],
      Duedate: ['',[Validators.required]],
      Status: ['',[Validators.required]]
      });
  }
getSubTask(taskid: number)
{
  this.subTasks = this.subtaskservice.getSubTask(taskid);
  //this.subTasks = this.allTask;
  // this.subtaskservice.getSubTask(taskid).subscribe(item=>{
  //   console.log(item);
  //   console.log(this.allTask);
  // });

}
markAsCompleted(subtaskid: number)
{
  this.subtaskservice.markAllAsComplete(subtaskid).subscribe(item=>{
    if(item==true)
    {
      this.refreshPage();
    }
  })
}
refreshPage() {
  window.location.reload();
 }
 public onFormSubmit(){
  let subtask = new Subtask();
  subtask.Title = this.subtaskForm.value.Title;
  subtask.Description = this.subtaskForm.value.Description;
  subtask.Duedate = this.subtaskForm.value.Duedate;
  subtask.Status = this.subtaskForm.value.Status;
  subtask.Taskid = Number(sessionStorage.getItem("taskid"));
  this.createSubTask(subtask);
}
createSubTask(subTask: Subtask)
{
  this.subtaskservice.createSubTask(subTask).subscribe(item=>{
    if(item==true)
    {
        this.refreshPage();
    }
  });
}

}
