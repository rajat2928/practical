import { Component, OnInit } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { Task } from '../task';
import { User } from '../user';
import {TaskServiceService} from '../Services/task-service.service'
import { Subtask } from '../subtask';
import { SubTaskService } from '../Services/sub-task.service';
import { Router } from '@angular/router';
import{FormBuilder,Validators, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  allTask:Observable<Task[]>;
  TaskList:Observable<Task[]>;
  List:Task[];
  statusList:Task[] = [];
  public isCollapsed = true;
  taskForm: any;
  All = "All";
  Completed="Complete";
  Incomplete="Incomplete";

  constructor(private formbuilder:FormBuilder,private taskservice: TaskServiceService, private subtaskservice: SubTaskService, private router: Router) {}

  ngOnInit(): void {
    this.getAllTask(Number(sessionStorage.getItem("UserID")));
    this.taskForm = this.formbuilder.group({
      Title: ['',[Validators.required]],
      Description: ['',[Validators.required]],
      Duedate: ['',[Validators.required]],
      Status: ['',[Validators.required]]
      });
  }
  getAllTask(userid: number)
  {
    this.allTask = this.taskservice.getAllTask(userid);
    this.TaskList = this.allTask;
  } 
  getStatusTask(status: string)
  {
    
    if(status.toUpperCase()=="all".toUpperCase())
    {
      this.TaskList = this.allTask;
    }
    else
    {
      this.taskservice.getAllTask(Number(sessionStorage.getItem("UserID"))).subscribe(x=>{
        this.List = x;
        this.List.forEach(x=>{
          if(x.Status.toUpperCase()==status.toUpperCase())
          {
            this.statusList.push(x);
          }
      });
      this.TaskList = of(this.statusList);
      this.statusList =[];
      });
      
      // this.allTask.forEach(item => {
      //   item.forEach(x =>{
      //     if(x.Status.toUpperCase()==status.toUpperCase())
      //     {
      //       this.statusList.push(x);
      //     }
      //   })
      // });
    }

  }
  subtaskList(taskid: number) : Subtask[]
  {
    var list;
    this.subtaskservice.getSubTask(taskid).subscribe(item=>{
      list = item
    });
    return list;
  }
  markAsCompleted(taskid: number)
  {
    this.taskservice.markAllAsComplete(taskid).subscribe(item=>{
      if(item==true)
      {
        this.refreshPage();
      }
    })
  }
  refreshPage() {
    window.location.reload();
   }
   subtaskNavigation(taskid: number, taskname: string)
   {
     sessionStorage.setItem("taskid",taskid.toString());
     sessionStorage.setItem("taskname",taskname.toString());
     this.router.navigate([('/subtask')]);
   }
   public onFormSubmit(){
    let task = new Task();
    task.Title = this.taskForm.value.Title;
    task.Description = this.taskForm.value.Description;
    task.Duedate = this.taskForm.value.Duedate;
    task.Status = this.taskForm.value.Status;
    task.UserId = Number(sessionStorage.getItem("UserID"));
    this.createSubTask(task);
  }
  createSubTask(task: Task)
  {
    this.taskservice.createTask(task).subscribe(item=>{
      if(item==true)
      {
          this.refreshPage();
      }
    });
  }
}
