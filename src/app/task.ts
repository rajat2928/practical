export class Task{
    Id : number;
    UserId : number;
    Title : string;
    Description : string;
    Duedate : string; 
    Status : string;
}